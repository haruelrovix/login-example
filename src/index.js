import React from 'react';
import ReactDOM from 'react-dom';
require('dotenv').config();

import App from './App';
import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
